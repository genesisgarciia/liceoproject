﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Usuario
    {
        public int id { get; set; }
        public String cedula { get; set; }
        public String nombre { get; set; }
        public String apellido { get; set; }
        public String cargo { get; set; }
        public String tipoUser { get; set; }



    }
}
