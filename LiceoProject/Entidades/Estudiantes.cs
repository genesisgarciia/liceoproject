﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Estudiantes
    {
        public int id { get; set; }
        public String cedula { get; set; }
        public String nombre { get; set; }
        public String apellidos { get; set; }
        public String residencia { get; set; }
        public String numeroEncargado { get; set; }
        public bool becaComedor{ get; set; }
        public bool becaTransporte{ get; set; }
        



    }
}
