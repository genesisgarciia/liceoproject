﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDiseño
{
    public partial class MenuUsuarios : Form
    {
        private Usuario user;
        public MenuUsuarios()
        {
            InitializeComponent();
            CargarTabla();
        }

        private void CargarTabla()
        {
            DataGridViewRow fila = new DataGridViewRow();
            fila.CreateCells(dgv);
            fila.Cells[0].Value = 1;
            fila.Cells[1].Value = "ADONAIS";
            fila.Cells[2].Value = "JIMENEZ VASQUEZ";
            fila.Cells[3].Value = "502690293";
            fila.Cells[4].Value = "DIRECTOR";
            fila.Cells[5].Value = "ADMIN";
            fila.Cells[6].Value = "SI";
            

            dgv.Rows.Add(fila);
            dgv.CurrentRow.Selected = false;

            DataGridViewRow fila2 = new DataGridViewRow();
            fila2.CreateCells(dgv);
            fila2.Cells[0].Value = 1;
            fila2.Cells[1].Value = "CRISTIAN";
            fila2.Cells[2].Value = "ROMERO VARGAS";
            fila2.Cells[3].Value = "207610854";
            fila2.Cells[4].Value = "AUXILAR ADMIN";
            fila2.Cells[5].Value = "ADMIN";
            fila2.Cells[6].Value = "SI";

            dgv.Rows.Add(fila2);
            dgv.CurrentRow.Selected = false;

        }

        private void CargarTabla2(string buscar)
        {
            DataGridViewRow fila = new DataGridViewRow();
            fila.CreateCells(dgv);
            fila.Cells[1].Value = "ADONAIS";
            fila.Cells[2].Value = "JIMENEZ VASQUEZ";
            fila.Cells[3].Value = "502690293";
            fila.Cells[4].Value = "DIRECTOR";
            fila.Cells[5].Value = "ADMIN";
            fila.Cells[6].Value = "SI";


            DataGridViewRow fila2 = new DataGridViewRow();
            fila2.CreateCells(dgv);
            fila2.Cells[0].Value = 1;
            fila2.Cells[1].Value = "CRISTIAN";
            fila2.Cells[2].Value = "ROMERO VARGAS";
            fila2.Cells[3].Value = "207610854";
            fila2.Cells[4].Value = "AUXILAR ADMIN";
            fila2.Cells[5].Value = "ADMIN";
            fila2.Cells[6].Value = "SI";

            dgv.CurrentRow.Selected = false;



            if (fila.Cells[3].Value.Equals(buscar))
            {
                dgv.Rows.Clear();
                dgv.Rows.Add(fila);
            }
            if (fila2.Cells[3].Value.Equals(buscar))
            {
                dgv.Rows.Clear();
                dgv.Rows.Add(fila2);
            }


        }

        public void limpiar()
        {
            txtNombre.Text = "";
            txtApellidos.Text = "";
            txtCedula.Text = "";
            txtCargo.Text = "";

        }

        private void cargarDatos()
        {
            if (dgv.SelectedRows.Count < 0)
            {
                MessageBox.Show("Selecione un estudiante de la tabla");
            }
            txtNombre.Text = user.nombre;
            txtCedula.Text = user.cedula;
            txtApellidos.Text = user.apellido;
            txtCargo.Text = user.cargo;

        }


        private void bunifuButton1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            AdminMenu am = new AdminMenu();
            am.Show();
            this.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuImageButton3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuTextBox3_TextChanged(object sender, EventArgs e)
        {
            CargarTabla2(txtBuscar.Text.Trim());
        }

        private void bunifuCustomDataGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dropActivo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
            DataGridViewRow fila = new DataGridViewRow();
            fila.CreateCells(dgv);
            fila.Cells[0].Value = 3;
            fila.Cells[1].Value = txtNombre.Text;
            fila.Cells[2].Value = txtApellidos.Text;
            fila.Cells[3].Value = txtCedula.Text;
            fila.Cells[4].Value = txtCargo.Text;
            fila.Cells[5].Value = dropActivo.SelectedItem.ToString();
            fila.Cells[6].Value = "SI";

            

            dgv.Rows.Add(fila);
            dgv.CurrentRow.Selected = false;

            if (dgv.SelectedRows.Count > 0)
            {
                dgv.Rows.Remove(dgv.CurrentRow);
            }

            limpiar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            dgv.CurrentRow.Selected = false;
            limpiar();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtCargo_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgv_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgv.SelectedRows.Count > 0 ? dgv.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                user = new Usuario();
                user.id = Convert.ToInt32(dgv[0, row].Value);
                user.nombre = dgv[1, row].Value.ToString();
                user.apellido = dgv[2, row].Value.ToString();
                user.cedula = dgv[3, row].Value.ToString();
                user.cargo = dgv[4, row].Value.ToString();
                user.tipoUser = dgv[5, row].Value.ToString();
                cargarDatos();
                
                



            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            dgv.Rows.Remove(dgv.CurrentRow);
            limpiar();
        }
    }
    
}

