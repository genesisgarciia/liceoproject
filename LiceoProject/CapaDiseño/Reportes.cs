﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CapaDiseño
{
    public partial class Reportes : Form
    {
        public Reportes()
        {
            InitializeComponent();
            
        }

        public void cargarChart()
        {

            if (becaTransporteCheck.Checked && becaComedorCheck.Checked) {
                int comedor, transporte;
                Random rnd = new Random();
                comedor = rnd.Next(10, 30);
                transporte = rnd.Next(10, 30);
                string[] series = { "Comedor", "Transporte" };
                int[] puntos = { comedor, transporte };

                for (int i = 0; i < series.Length; i++)
                {
                    Series serie = chart1.Series.Add(series[i]);

                    serie.Label = puntos[i].ToString();
                    serie.Points.Add(puntos[i]);
                }


            }

            else if (!becaTransporteCheck.Checked && becaComedorCheck.Checked) {
                int comedor;
                Random rnd = new Random();
                comedor = rnd.Next(10, 12);
                string[] series = { "Comedor" };
                int[] puntos = { comedor };

                for (int i = 0; i < series.Length; i++)
                {
                    Series serie = chart1.Series.Add(series[i]);

                    serie.Label = puntos[i].ToString();
                    serie.Points.Add(puntos[i]);
                }

            }
            else if (becaTransporteCheck.Checked && !becaComedorCheck.Checked)
            {
                int transporte;
                Random rnd = new Random();
                transporte = rnd.Next(1, 12);
                string[] series = { "transporte" };
                int[] puntos = { transporte };

                for (int i = 0; i < series.Length; i++)
                {
                    Series serie = chart1.Series.Add(series[i]);

                    serie.Label = puntos[i].ToString();
                    serie.Points.Add(puntos[i]);
                }

            }


        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            AdminMenu am = new AdminMenu();
            am.Show();
            this.Hide();
        }

        private void bunifuImageButton3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();
            cargarChart();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void becaTransporteCheck_CheckedChanged(object sender, Bunifu.UI.WinForms.BunifuCheckBox.CheckedChangedEventArgs e)
        {

        }
    }
}
