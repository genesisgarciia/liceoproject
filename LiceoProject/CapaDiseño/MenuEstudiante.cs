﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDiseño
{
    public partial class MenuEstudiante : Form
    {
        private Estudiantes es;
        public MenuEstudiante()
        {
            InitializeComponent();
            es = new Estudiantes();
            CargarTabla();
        }


        private void CargarTabla()
        {
            
            DataGridViewRow fila = new DataGridViewRow();
            fila.CreateCells(dgv);
            fila.Cells[0].Value = 1;
            fila.Cells[1].Value = "GENESIS";
            fila.Cells[2].Value = "GARCIA ARAYA";
            fila.Cells[3].Value = "702480818";
            fila.Cells[4].Value = "3123223";
            fila.Cells[5].Value = "Sarapiqui";
            fila.Cells[6].Value = "SI";
            fila.Cells[7].Value = "NO";

            dgv.Rows.Add(fila);

            DataGridViewRow fila2 = new DataGridViewRow();
            fila2.CreateCells(dgv);
            fila2.Cells[0].Value = 1;
            fila2.Cells[1].Value = "DANIEL";
            fila2.Cells[2].Value = "VARGAS SIBAJA";
            fila2.Cells[3].Value = "32321331";
            fila2.Cells[4].Value = "54535";
            fila2.Cells[5].Value = "colonia puntarenas";
            fila2.Cells[6].Value = "SI";
            fila2.Cells[7].Value = "SI";

            dgv.Rows.Add(fila2);
            dgv.CurrentRow.Selected = false;

        }

        private void CargarTabla2(string buscar)
        {
            DataGridViewRow fila = new DataGridViewRow();
            fila.CreateCells(dgv);
            fila.Cells[0].Value = 1;
            fila.Cells[1].Value = "GENESIS";
            fila.Cells[2].Value = "GARCIA ARAYA";
            fila.Cells[3].Value = "702480818";
            fila.Cells[4].Value = "3123223";
            fila.Cells[5].Value = "Sarapiqui";
            fila.Cells[6].Value = "SI";
            fila.Cells[7].Value = "NO";


            DataGridViewRow fila2 = new DataGridViewRow();
            fila2.CreateCells(dgv);
            fila2.Cells[0].Value = 1;
            fila2.Cells[1].Value = "DANIEL";
            fila2.Cells[2].Value = "VARGAS SIBAJA";
            fila2.Cells[3].Value = "207610854";
            fila2.Cells[4].Value = "54535";
            fila2.Cells[5].Value = "colonia puntarenas";
            fila2.Cells[6].Value = "NO";
            fila2.Cells[7].Value = "SI";


            if(fila.Cells[3].Value.Equals(buscar))
            {
                dgv.Rows.Clear();
                dgv.Rows.Add(fila);
            }
            if (fila2.Cells[3].Value.Equals(buscar))
            {
                dgv.Rows.Clear();
                dgv.Rows.Add(fila2);
            }


        }


        private void bunifuCards1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            DataGridViewRow fila = new DataGridViewRow();
            fila.CreateCells(dgv);
            string transporte = "";
            string comedor = "";
            if (checkTransporte.Checked)
            {
                transporte = "SI";
            }
            else
            {
                transporte = "NO";
            }
            if (checkComedor.Checked)
            {
                comedor = "SI";
            }
            else
            {
                comedor = "NO";
            }
            fila.Cells[0].Value = 3;
            fila.Cells[1].Value = txtNombre.Text;
            fila.Cells[2].Value = txtApellidos.Text;
            fila.Cells[3].Value = txtCedula.Text;
            fila.Cells[4].Value = txtEncargado.Text;
            fila.Cells[5].Value = txtResidencia.Text;
            fila.Cells[6].Value = transporte;
            fila.Cells[7].Value = comedor;

            dgv.Rows.Add(fila);

            if (dgv.SelectedRows.Count > 0)
            {
                dgv.Rows.Remove(dgv.CurrentRow);
            }

            limpiar();
        }


        public void limpiar()
        {
            txtNombre.Text = "";
            txtApellidos.Text = "";
            txtCedula.Text = "";
            txtEncargado.Text = "";
            txtResidencia.Text = "";
            checkComedor.Checked = false;
            checkTransporte.Checked = false;
        }


        private void cargarDatos()
        {
            if (dgv.SelectedRows.Count < 0)
            {
                MessageBox.Show("Selecione un estudiante de la tabla");
            }
            txtNombre.Text = es.nombre;
            txtCedula.Text = es.cedula;
            txtApellidos.Text = es.apellidos;
            txtResidencia.Text = es.residencia;
            txtEncargado.Text = es.numeroEncargado;
            if(es.becaComedor == true)
            {
                checkComedor.Checked = true;
            }
            else
            {
                checkComedor.Checked = false;
            }

            if (es.becaTransporte == true)
            {
                checkTransporte.Checked = true;
            }
            else
            {
                checkTransporte.Checked = false;
            }

        }

        private void bunifuButton1_Click(object sender, EventArgs e)
        {
            dgv.Rows.Remove(dgv.CurrentRow);
            limpiar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            dgv.CurrentRow.Selected = false;
            limpiar();
        }

        private void dgv_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgv.SelectedRows.Count > 0 ? dgv.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                bool transporte = true;
                bool comedor = true;
                if (dgv[6, row].Value.ToString() == "SI")
                {
                    transporte = true;
                }
                else
                {
                    transporte = false;
                }

                if(dgv[7, row].Value.ToString() == "SI")
                {
                    comedor = true;
                }
                else
                {
                    comedor = false;
                }

                es = new Estudiantes();
                es.id = Convert.ToInt32(dgv[0, row].Value);
                es.nombre = dgv[1, row].Value.ToString();
                es.apellidos = dgv[2, row].Value.ToString();
                es.cedula = dgv[3, row].Value.ToString();
                es.numeroEncargado = dgv[4, row].Value.ToString();
                es.residencia = dgv[5, row].Value.ToString();
                es.becaTransporte = transporte;
                es.becaComedor = comedor;
                cargarDatos();
            }
        }

        private void bunifuTextBox3_TextChange(object sender, EventArgs e)
        {
            CargarTabla2(txtBuscar.Text.Trim());
            
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            AdminMenu am = new AdminMenu();
            am.Show();
            this.Hide();
        }

        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuImageButton3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
