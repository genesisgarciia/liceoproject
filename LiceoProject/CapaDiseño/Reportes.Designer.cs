﻿namespace CapaDiseño
{
    partial class Reportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reportes));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuCustomDataGrid1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.residencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.becaTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.becaComedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bunifuDatepicker1 = new Bunifu.Framework.UI.BunifuDatepicker();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.becaComedorCheck = new Bunifu.UI.WinForms.BunifuCheckBox();
            this.becaTransporteCheck = new Bunifu.UI.WinForms.BunifuCheckBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnNuevo = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuCustomDataGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 10;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.bunifuImageButton2);
            this.panel1.Controls.Add(this.bunifuImageButton3);
            this.panel1.Controls.Add(this.bunifuImageButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1258, 72);
            this.panel1.TabIndex = 1;
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(1175, 12);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(59, 42);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 10;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 10;
            this.bunifuImageButton2.Click += new System.EventHandler(this.bunifuImageButton2_Click);
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.Image")));
            this.bunifuImageButton3.ImageActive = null;
            this.bunifuImageButton3.Location = new System.Drawing.Point(1110, 12);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(70, 57);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton3.TabIndex = 5;
            this.bunifuImageButton3.TabStop = false;
            this.bunifuImageButton3.Zoom = 10;
            this.bunifuImageButton3.Click += new System.EventHandler(this.bunifuImageButton3_Click);
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(0, 3);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(59, 66);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 3;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // bunifuCustomDataGrid1
            // 
            this.bunifuCustomDataGrid1.AllowUserToAddRows = false;
            this.bunifuCustomDataGrid1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuCustomDataGrid1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.bunifuCustomDataGrid1.BackgroundColor = System.Drawing.Color.White;
            this.bunifuCustomDataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bunifuCustomDataGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bunifuCustomDataGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.bunifuCustomDataGrid1.ColumnHeadersHeight = 45;
            this.bunifuCustomDataGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.bunifuCustomDataGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.nombre,
            this.apellidos,
            this.cedula,
            this.numero,
            this.residencia,
            this.becaTransporte,
            this.becaComedor,
            this.fecha});
            this.bunifuCustomDataGrid1.DoubleBuffered = true;
            this.bunifuCustomDataGrid1.EnableHeadersVisualStyles = false;
            this.bunifuCustomDataGrid1.GridColor = System.Drawing.Color.White;
            this.bunifuCustomDataGrid1.HeaderBgColor = System.Drawing.Color.DodgerBlue;
            this.bunifuCustomDataGrid1.HeaderForeColor = System.Drawing.Color.White;
            this.bunifuCustomDataGrid1.Location = new System.Drawing.Point(13, 169);
            this.bunifuCustomDataGrid1.Name = "bunifuCustomDataGrid1";
            this.bunifuCustomDataGrid1.ReadOnly = true;
            this.bunifuCustomDataGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.bunifuCustomDataGrid1.RowHeadersWidth = 51;
            this.bunifuCustomDataGrid1.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomDataGrid1.RowTemplate.Height = 24;
            this.bunifuCustomDataGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.bunifuCustomDataGrid1.Size = new System.Drawing.Size(800, 560);
            this.bunifuCustomDataGrid1.TabIndex = 6;
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 125;
            // 
            // nombre
            // 
            this.nombre.HeaderText = "Nombre";
            this.nombre.MinimumWidth = 6;
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            this.nombre.Width = 125;
            // 
            // apellidos
            // 
            this.apellidos.HeaderText = "Apellidos";
            this.apellidos.MinimumWidth = 6;
            this.apellidos.Name = "apellidos";
            this.apellidos.ReadOnly = true;
            this.apellidos.Width = 125;
            // 
            // cedula
            // 
            this.cedula.HeaderText = "Cédula";
            this.cedula.MinimumWidth = 6;
            this.cedula.Name = "cedula";
            this.cedula.ReadOnly = true;
            this.cedula.Width = 125;
            // 
            // numero
            // 
            this.numero.HeaderText = "Número encargado";
            this.numero.MinimumWidth = 6;
            this.numero.Name = "numero";
            this.numero.ReadOnly = true;
            this.numero.Width = 125;
            // 
            // residencia
            // 
            this.residencia.HeaderText = "Residencia";
            this.residencia.MinimumWidth = 6;
            this.residencia.Name = "residencia";
            this.residencia.ReadOnly = true;
            this.residencia.Width = 125;
            // 
            // becaTransporte
            // 
            this.becaTransporte.HeaderText = "Beca transporte";
            this.becaTransporte.MinimumWidth = 6;
            this.becaTransporte.Name = "becaTransporte";
            this.becaTransporte.ReadOnly = true;
            this.becaTransporte.Width = 125;
            // 
            // becaComedor
            // 
            this.becaComedor.HeaderText = "Beca comedor";
            this.becaComedor.MinimumWidth = 6;
            this.becaComedor.Name = "becaComedor";
            this.becaComedor.ReadOnly = true;
            this.becaComedor.Width = 125;
            // 
            // fecha
            // 
            this.fecha.HeaderText = "Fecha";
            this.fecha.MinimumWidth = 6;
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            this.fecha.Width = 125;
            // 
            // bunifuDatepicker1
            // 
            this.bunifuDatepicker1.BackColor = System.Drawing.Color.White;
            this.bunifuDatepicker1.BorderRadius = 7;
            this.bunifuDatepicker1.ForeColor = System.Drawing.Color.Gray;
            this.bunifuDatepicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.bunifuDatepicker1.FormatCustom = null;
            this.bunifuDatepicker1.Location = new System.Drawing.Point(13, 92);
            this.bunifuDatepicker1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuDatepicker1.Name = "bunifuDatepicker1";
            this.bunifuDatepicker1.Size = new System.Drawing.Size(404, 44);
            this.bunifuDatepicker1.TabIndex = 9;
            this.bunifuDatepicker1.Value = new System.DateTime(2020, 8, 22, 17, 35, 30, 528);
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(674, 102);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(175, 25);
            this.bunifuCustomLabel2.TabIndex = 19;
            this.bunifuCustomLabel2.Text = "Beca comedor";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(458, 102);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(175, 25);
            this.bunifuCustomLabel1.TabIndex = 18;
            this.bunifuCustomLabel1.Text = "Beca transporte";
            // 
            // becaComedorCheck
            // 
            this.becaComedorCheck.AllowBindingControlAnimation = true;
            this.becaComedorCheck.AllowBindingControlColorChanges = false;
            this.becaComedorCheck.AllowBindingControlLocation = true;
            this.becaComedorCheck.AllowCheckBoxAnimation = false;
            this.becaComedorCheck.AllowCheckmarkAnimation = true;
            this.becaComedorCheck.AllowOnHoverStates = true;
            this.becaComedorCheck.AutoCheck = true;
            this.becaComedorCheck.BackColor = System.Drawing.Color.Transparent;
            this.becaComedorCheck.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("becaComedorCheck.BackgroundImage")));
            this.becaComedorCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.becaComedorCheck.BindingControl = null;
            this.becaComedorCheck.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.becaComedorCheck.Checked = false;
            this.becaComedorCheck.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.becaComedorCheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.becaComedorCheck.CustomCheckmarkImage = null;
            this.becaComedorCheck.Location = new System.Drawing.Point(643, 102);
            this.becaComedorCheck.MinimumSize = new System.Drawing.Size(17, 17);
            this.becaComedorCheck.Name = "becaComedorCheck";
            this.becaComedorCheck.OnCheck.BorderColor = System.Drawing.Color.DodgerBlue;
            this.becaComedorCheck.OnCheck.BorderRadius = 1;
            this.becaComedorCheck.OnCheck.BorderThickness = 2;
            this.becaComedorCheck.OnCheck.CheckBoxColor = System.Drawing.Color.DodgerBlue;
            this.becaComedorCheck.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.becaComedorCheck.OnCheck.CheckmarkThickness = 2;
            this.becaComedorCheck.OnDisable.BorderColor = System.Drawing.Color.LightGray;
            this.becaComedorCheck.OnDisable.BorderRadius = 1;
            this.becaComedorCheck.OnDisable.BorderThickness = 2;
            this.becaComedorCheck.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.becaComedorCheck.OnDisable.CheckmarkColor = System.Drawing.Color.LightGray;
            this.becaComedorCheck.OnDisable.CheckmarkThickness = 2;
            this.becaComedorCheck.OnHoverChecked.BorderColor = System.Drawing.Color.LightSkyBlue;
            this.becaComedorCheck.OnHoverChecked.BorderRadius = 1;
            this.becaComedorCheck.OnHoverChecked.BorderThickness = 2;
            this.becaComedorCheck.OnHoverChecked.CheckBoxColor = System.Drawing.Color.LightSkyBlue;
            this.becaComedorCheck.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.becaComedorCheck.OnHoverChecked.CheckmarkThickness = 2;
            this.becaComedorCheck.OnHoverUnchecked.BorderColor = System.Drawing.Color.DodgerBlue;
            this.becaComedorCheck.OnHoverUnchecked.BorderRadius = 1;
            this.becaComedorCheck.OnHoverUnchecked.BorderThickness = 2;
            this.becaComedorCheck.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.becaComedorCheck.OnUncheck.BorderColor = System.Drawing.Color.DodgerBlue;
            this.becaComedorCheck.OnUncheck.BorderRadius = 1;
            this.becaComedorCheck.OnUncheck.BorderThickness = 2;
            this.becaComedorCheck.OnUncheck.CheckBoxColor = System.Drawing.Color.Transparent;
            this.becaComedorCheck.Size = new System.Drawing.Size(25, 25);
            this.becaComedorCheck.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Round;
            this.becaComedorCheck.TabIndex = 17;
            this.becaComedorCheck.ThreeState = false;
            this.becaComedorCheck.ToolTipText = null;
            // 
            // becaTransporteCheck
            // 
            this.becaTransporteCheck.AllowBindingControlAnimation = true;
            this.becaTransporteCheck.AllowBindingControlColorChanges = false;
            this.becaTransporteCheck.AllowBindingControlLocation = true;
            this.becaTransporteCheck.AllowCheckBoxAnimation = false;
            this.becaTransporteCheck.AllowCheckmarkAnimation = true;
            this.becaTransporteCheck.AllowOnHoverStates = true;
            this.becaTransporteCheck.AutoCheck = true;
            this.becaTransporteCheck.BackColor = System.Drawing.Color.Transparent;
            this.becaTransporteCheck.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("becaTransporteCheck.BackgroundImage")));
            this.becaTransporteCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.becaTransporteCheck.BindingControl = null;
            this.becaTransporteCheck.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.becaTransporteCheck.Checked = false;
            this.becaTransporteCheck.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.becaTransporteCheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.becaTransporteCheck.CustomCheckmarkImage = null;
            this.becaTransporteCheck.Location = new System.Drawing.Point(427, 102);
            this.becaTransporteCheck.MinimumSize = new System.Drawing.Size(17, 17);
            this.becaTransporteCheck.Name = "becaTransporteCheck";
            this.becaTransporteCheck.OnCheck.BorderColor = System.Drawing.Color.DodgerBlue;
            this.becaTransporteCheck.OnCheck.BorderRadius = 1;
            this.becaTransporteCheck.OnCheck.BorderThickness = 2;
            this.becaTransporteCheck.OnCheck.CheckBoxColor = System.Drawing.Color.DodgerBlue;
            this.becaTransporteCheck.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.becaTransporteCheck.OnCheck.CheckmarkThickness = 2;
            this.becaTransporteCheck.OnDisable.BorderColor = System.Drawing.Color.LightGray;
            this.becaTransporteCheck.OnDisable.BorderRadius = 1;
            this.becaTransporteCheck.OnDisable.BorderThickness = 2;
            this.becaTransporteCheck.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.becaTransporteCheck.OnDisable.CheckmarkColor = System.Drawing.Color.LightGray;
            this.becaTransporteCheck.OnDisable.CheckmarkThickness = 2;
            this.becaTransporteCheck.OnHoverChecked.BorderColor = System.Drawing.Color.LightSkyBlue;
            this.becaTransporteCheck.OnHoverChecked.BorderRadius = 1;
            this.becaTransporteCheck.OnHoverChecked.BorderThickness = 2;
            this.becaTransporteCheck.OnHoverChecked.CheckBoxColor = System.Drawing.Color.LightSkyBlue;
            this.becaTransporteCheck.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.becaTransporteCheck.OnHoverChecked.CheckmarkThickness = 2;
            this.becaTransporteCheck.OnHoverUnchecked.BorderColor = System.Drawing.Color.DodgerBlue;
            this.becaTransporteCheck.OnHoverUnchecked.BorderRadius = 1;
            this.becaTransporteCheck.OnHoverUnchecked.BorderThickness = 2;
            this.becaTransporteCheck.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.becaTransporteCheck.OnUncheck.BorderColor = System.Drawing.Color.DodgerBlue;
            this.becaTransporteCheck.OnUncheck.BorderRadius = 1;
            this.becaTransporteCheck.OnUncheck.BorderThickness = 2;
            this.becaTransporteCheck.OnUncheck.CheckBoxColor = System.Drawing.Color.Transparent;
            this.becaTransporteCheck.Size = new System.Drawing.Size(25, 25);
            this.becaTransporteCheck.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Round;
            this.becaTransporteCheck.TabIndex = 16;
            this.becaTransporteCheck.ThreeState = false;
            this.becaTransporteCheck.ToolTipText = null;
            this.becaTransporteCheck.CheckedChanged += new System.EventHandler<Bunifu.UI.WinForms.BunifuCheckBox.CheckedChangedEventArgs>(this.becaTransporteCheck_CheckedChanged);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(819, 169);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(427, 560);
            this.chart1.TabIndex = 20;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.AllowToggling = false;
            this.btnNuevo.AnimationSpeed = 200;
            this.btnNuevo.AutoGenerateColors = false;
            this.btnNuevo.BackColor = System.Drawing.Color.Transparent;
            this.btnNuevo.BackColor1 = System.Drawing.Color.DodgerBlue;
            this.btnNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNuevo.BackgroundImage")));
            this.btnNuevo.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btnNuevo.ButtonText = "Generar";
            this.btnNuevo.ButtonTextMarginLeft = 0;
            this.btnNuevo.ColorContrastOnClick = 45;
            this.btnNuevo.ColorContrastOnHover = 45;
            this.btnNuevo.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.btnNuevo.CustomizableEdges = borderEdges1;
            this.btnNuevo.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnNuevo.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btnNuevo.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btnNuevo.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btnNuevo.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btnNuevo.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.ForeColor = System.Drawing.Color.White;
            this.btnNuevo.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btnNuevo.IconMarginLeft = 11;
            this.btnNuevo.IconPadding = 10;
            this.btnNuevo.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnNuevo.IdleBorderColor = System.Drawing.Color.DodgerBlue;
            this.btnNuevo.IdleBorderRadius = 7;
            this.btnNuevo.IdleBorderThickness = 1;
            this.btnNuevo.IdleFillColor = System.Drawing.Color.DodgerBlue;
            this.btnNuevo.IdleIconLeftImage = null;
            this.btnNuevo.IdleIconRightImage = null;
            this.btnNuevo.IndicateFocus = false;
            this.btnNuevo.Location = new System.Drawing.Point(857, 92);
            this.btnNuevo.Name = "btnNuevo";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties1.BorderRadius = 7;
            stateProperties1.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties1.BorderThickness = 1;
            stateProperties1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties1.ForeColor = System.Drawing.Color.White;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = null;
            this.btnNuevo.onHoverState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties2.BorderRadius = 7;
            stateProperties2.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties2.BorderThickness = 1;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties2.ForeColor = System.Drawing.Color.White;
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = null;
            this.btnNuevo.OnPressedState = stateProperties2;
            this.btnNuevo.Size = new System.Drawing.Size(150, 45);
            this.btnNuevo.TabIndex = 22;
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnNuevo.TextMarginLeft = 0;
            this.btnNuevo.UseDefaultRadiusAndThickness = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // Reportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(249)))));
            this.ClientSize = new System.Drawing.Size(1258, 761);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.becaComedorCheck);
            this.Controls.Add(this.becaTransporteCheck);
            this.Controls.Add(this.bunifuDatepicker1);
            this.Controls.Add(this.bunifuCustomDataGrid1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Reportes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reportes";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuCustomDataGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid bunifuCustomDataGrid1;
        private Bunifu.Framework.UI.BunifuDatepicker bunifuDatepicker1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.UI.WinForms.BunifuCheckBox becaComedorCheck;
        private Bunifu.UI.WinForms.BunifuCheckBox becaTransporteCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn residencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn becaTransporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn becaComedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnNuevo;
    }
}