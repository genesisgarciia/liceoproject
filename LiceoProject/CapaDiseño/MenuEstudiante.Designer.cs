﻿namespace CapaDiseño
{
    partial class MenuEstudiante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuEstudiante));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties5 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties6 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties7 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties8 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties9 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties10 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties11 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties12 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties13 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties14 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties15 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties16 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties17 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties18 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties19 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties20 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties21 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties22 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties23 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties24 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties25 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties26 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties27 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties28 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties29 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties stateProperties30 = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox.StateProperties();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnNuevo = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnEliminar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.checkComedor = new Bunifu.UI.WinForms.BunifuCheckBox();
            this.checkTransporte = new Bunifu.UI.WinForms.BunifuCheckBox();
            this.txtResidencia = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.btnGuardar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.txtApellidos = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtCedula = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtEncargado = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtNombre = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.residencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.becaTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.becaComedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.txtBuscar = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = null;
            this.bunifuDragControl1.Vertical = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.btnNuevo);
            this.panel2.Controls.Add(this.btnEliminar);
            this.panel2.Controls.Add(this.bunifuCustomLabel2);
            this.panel2.Controls.Add(this.bunifuCustomLabel1);
            this.panel2.Controls.Add(this.checkComedor);
            this.panel2.Controls.Add(this.checkTransporte);
            this.panel2.Controls.Add(this.txtResidencia);
            this.panel2.Controls.Add(this.btnGuardar);
            this.panel2.Controls.Add(this.txtApellidos);
            this.panel2.Controls.Add(this.txtCedula);
            this.panel2.Controls.Add(this.txtEncargado);
            this.panel2.Controls.Add(this.txtNombre);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(1007, 113);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(521, 627);
            this.panel2.TabIndex = 6;
            // 
            // btnNuevo
            // 
            this.btnNuevo.AllowToggling = false;
            this.btnNuevo.AnimationSpeed = 200;
            this.btnNuevo.AutoGenerateColors = false;
            this.btnNuevo.BackColor = System.Drawing.Color.Transparent;
            this.btnNuevo.BackColor1 = System.Drawing.Color.DodgerBlue;
            this.btnNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNuevo.BackgroundImage")));
            this.btnNuevo.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btnNuevo.ButtonText = "Nuevo";
            this.btnNuevo.ButtonTextMarginLeft = 0;
            this.btnNuevo.ColorContrastOnClick = 45;
            this.btnNuevo.ColorContrastOnHover = 45;
            this.btnNuevo.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.btnNuevo.CustomizableEdges = borderEdges1;
            this.btnNuevo.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnNuevo.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btnNuevo.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btnNuevo.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btnNuevo.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btnNuevo.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.ForeColor = System.Drawing.Color.White;
            this.btnNuevo.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btnNuevo.IconMarginLeft = 11;
            this.btnNuevo.IconPadding = 10;
            this.btnNuevo.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnNuevo.IdleBorderColor = System.Drawing.Color.DodgerBlue;
            this.btnNuevo.IdleBorderRadius = 7;
            this.btnNuevo.IdleBorderThickness = 1;
            this.btnNuevo.IdleFillColor = System.Drawing.Color.DodgerBlue;
            this.btnNuevo.IdleIconLeftImage = null;
            this.btnNuevo.IdleIconRightImage = null;
            this.btnNuevo.IndicateFocus = false;
            this.btnNuevo.Location = new System.Drawing.Point(17, 555);
            this.btnNuevo.Name = "btnNuevo";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties1.BorderRadius = 7;
            stateProperties1.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties1.BorderThickness = 1;
            stateProperties1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties1.ForeColor = System.Drawing.Color.White;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = null;
            this.btnNuevo.onHoverState = stateProperties1;
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties2.BorderRadius = 7;
            stateProperties2.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties2.BorderThickness = 1;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties2.ForeColor = System.Drawing.Color.White;
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = null;
            this.btnNuevo.OnPressedState = stateProperties2;
            this.btnNuevo.Size = new System.Drawing.Size(150, 45);
            this.btnNuevo.TabIndex = 17;
            this.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnNuevo.TextMarginLeft = 0;
            this.btnNuevo.UseDefaultRadiusAndThickness = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.AllowToggling = false;
            this.btnEliminar.AnimationSpeed = 200;
            this.btnEliminar.AutoGenerateColors = false;
            this.btnEliminar.BackColor = System.Drawing.Color.Transparent;
            this.btnEliminar.BackColor1 = System.Drawing.Color.DodgerBlue;
            this.btnEliminar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar.BackgroundImage")));
            this.btnEliminar.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btnEliminar.ButtonText = "Eliminar";
            this.btnEliminar.ButtonTextMarginLeft = 0;
            this.btnEliminar.ColorContrastOnClick = 45;
            this.btnEliminar.ColorContrastOnHover = 45;
            this.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges2.BottomLeft = true;
            borderEdges2.BottomRight = true;
            borderEdges2.TopLeft = true;
            borderEdges2.TopRight = true;
            this.btnEliminar.CustomizableEdges = borderEdges2;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnEliminar.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btnEliminar.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btnEliminar.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btnEliminar.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btnEliminar.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.Color.White;
            this.btnEliminar.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.IconMarginLeft = 11;
            this.btnEliminar.IconPadding = 10;
            this.btnEliminar.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.IdleBorderColor = System.Drawing.Color.DodgerBlue;
            this.btnEliminar.IdleBorderRadius = 7;
            this.btnEliminar.IdleBorderThickness = 1;
            this.btnEliminar.IdleFillColor = System.Drawing.Color.DodgerBlue;
            this.btnEliminar.IdleIconLeftImage = null;
            this.btnEliminar.IdleIconRightImage = null;
            this.btnEliminar.IndicateFocus = false;
            this.btnEliminar.Location = new System.Drawing.Point(361, 555);
            this.btnEliminar.Name = "btnEliminar";
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties3.BorderRadius = 7;
            stateProperties3.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties3.BorderThickness = 1;
            stateProperties3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties3.ForeColor = System.Drawing.Color.White;
            stateProperties3.IconLeftImage = null;
            stateProperties3.IconRightImage = null;
            this.btnEliminar.onHoverState = stateProperties3;
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties4.BorderRadius = 7;
            stateProperties4.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties4.BorderThickness = 1;
            stateProperties4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties4.ForeColor = System.Drawing.Color.White;
            stateProperties4.IconLeftImage = null;
            stateProperties4.IconRightImage = null;
            this.btnEliminar.OnPressedState = stateProperties4;
            this.btnEliminar.Size = new System.Drawing.Size(149, 45);
            this.btnEliminar.TabIndex = 16;
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEliminar.TextMarginLeft = 0;
            this.btnEliminar.UseDefaultRadiusAndThickness = true;
            this.btnEliminar.Click += new System.EventHandler(this.bunifuButton1_Click);
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(100, 490);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(175, 25);
            this.bunifuCustomLabel2.TabIndex = 15;
            this.bunifuCustomLabel2.Text = "Beca comedor";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(100, 438);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(175, 25);
            this.bunifuCustomLabel1.TabIndex = 14;
            this.bunifuCustomLabel1.Text = "Beca transporte";
            // 
            // checkComedor
            // 
            this.checkComedor.AllowBindingControlAnimation = true;
            this.checkComedor.AllowBindingControlColorChanges = false;
            this.checkComedor.AllowBindingControlLocation = true;
            this.checkComedor.AllowCheckBoxAnimation = false;
            this.checkComedor.AllowCheckmarkAnimation = true;
            this.checkComedor.AllowOnHoverStates = true;
            this.checkComedor.AutoCheck = true;
            this.checkComedor.BackColor = System.Drawing.Color.Transparent;
            this.checkComedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("checkComedor.BackgroundImage")));
            this.checkComedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.checkComedor.BindingControl = null;
            this.checkComedor.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.checkComedor.Checked = false;
            this.checkComedor.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.checkComedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkComedor.CustomCheckmarkImage = null;
            this.checkComedor.Location = new System.Drawing.Point(69, 490);
            this.checkComedor.MinimumSize = new System.Drawing.Size(17, 17);
            this.checkComedor.Name = "checkComedor";
            this.checkComedor.OnCheck.BorderColor = System.Drawing.Color.DodgerBlue;
            this.checkComedor.OnCheck.BorderRadius = 1;
            this.checkComedor.OnCheck.BorderThickness = 2;
            this.checkComedor.OnCheck.CheckBoxColor = System.Drawing.Color.DodgerBlue;
            this.checkComedor.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.checkComedor.OnCheck.CheckmarkThickness = 2;
            this.checkComedor.OnDisable.BorderColor = System.Drawing.Color.LightGray;
            this.checkComedor.OnDisable.BorderRadius = 1;
            this.checkComedor.OnDisable.BorderThickness = 2;
            this.checkComedor.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.checkComedor.OnDisable.CheckmarkColor = System.Drawing.Color.LightGray;
            this.checkComedor.OnDisable.CheckmarkThickness = 2;
            this.checkComedor.OnHoverChecked.BorderColor = System.Drawing.Color.LightSkyBlue;
            this.checkComedor.OnHoverChecked.BorderRadius = 1;
            this.checkComedor.OnHoverChecked.BorderThickness = 2;
            this.checkComedor.OnHoverChecked.CheckBoxColor = System.Drawing.Color.LightSkyBlue;
            this.checkComedor.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.checkComedor.OnHoverChecked.CheckmarkThickness = 2;
            this.checkComedor.OnHoverUnchecked.BorderColor = System.Drawing.Color.DodgerBlue;
            this.checkComedor.OnHoverUnchecked.BorderRadius = 1;
            this.checkComedor.OnHoverUnchecked.BorderThickness = 2;
            this.checkComedor.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.checkComedor.OnUncheck.BorderColor = System.Drawing.Color.DodgerBlue;
            this.checkComedor.OnUncheck.BorderRadius = 1;
            this.checkComedor.OnUncheck.BorderThickness = 2;
            this.checkComedor.OnUncheck.CheckBoxColor = System.Drawing.Color.Transparent;
            this.checkComedor.Size = new System.Drawing.Size(25, 25);
            this.checkComedor.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Round;
            this.checkComedor.TabIndex = 13;
            this.checkComedor.ThreeState = false;
            this.checkComedor.ToolTipText = null;
            // 
            // checkTransporte
            // 
            this.checkTransporte.AllowBindingControlAnimation = true;
            this.checkTransporte.AllowBindingControlColorChanges = false;
            this.checkTransporte.AllowBindingControlLocation = true;
            this.checkTransporte.AllowCheckBoxAnimation = false;
            this.checkTransporte.AllowCheckmarkAnimation = true;
            this.checkTransporte.AllowOnHoverStates = true;
            this.checkTransporte.AutoCheck = true;
            this.checkTransporte.BackColor = System.Drawing.Color.Transparent;
            this.checkTransporte.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("checkTransporte.BackgroundImage")));
            this.checkTransporte.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.checkTransporte.BindingControl = null;
            this.checkTransporte.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.checkTransporte.Checked = false;
            this.checkTransporte.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.checkTransporte.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkTransporte.CustomCheckmarkImage = null;
            this.checkTransporte.Location = new System.Drawing.Point(69, 438);
            this.checkTransporte.MinimumSize = new System.Drawing.Size(17, 17);
            this.checkTransporte.Name = "checkTransporte";
            this.checkTransporte.OnCheck.BorderColor = System.Drawing.Color.DodgerBlue;
            this.checkTransporte.OnCheck.BorderRadius = 1;
            this.checkTransporte.OnCheck.BorderThickness = 2;
            this.checkTransporte.OnCheck.CheckBoxColor = System.Drawing.Color.DodgerBlue;
            this.checkTransporte.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.checkTransporte.OnCheck.CheckmarkThickness = 2;
            this.checkTransporte.OnDisable.BorderColor = System.Drawing.Color.LightGray;
            this.checkTransporte.OnDisable.BorderRadius = 1;
            this.checkTransporte.OnDisable.BorderThickness = 2;
            this.checkTransporte.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.checkTransporte.OnDisable.CheckmarkColor = System.Drawing.Color.LightGray;
            this.checkTransporte.OnDisable.CheckmarkThickness = 2;
            this.checkTransporte.OnHoverChecked.BorderColor = System.Drawing.Color.LightSkyBlue;
            this.checkTransporte.OnHoverChecked.BorderRadius = 1;
            this.checkTransporte.OnHoverChecked.BorderThickness = 2;
            this.checkTransporte.OnHoverChecked.CheckBoxColor = System.Drawing.Color.LightSkyBlue;
            this.checkTransporte.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.checkTransporte.OnHoverChecked.CheckmarkThickness = 2;
            this.checkTransporte.OnHoverUnchecked.BorderColor = System.Drawing.Color.DodgerBlue;
            this.checkTransporte.OnHoverUnchecked.BorderRadius = 1;
            this.checkTransporte.OnHoverUnchecked.BorderThickness = 2;
            this.checkTransporte.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.checkTransporte.OnUncheck.BorderColor = System.Drawing.Color.DodgerBlue;
            this.checkTransporte.OnUncheck.BorderRadius = 1;
            this.checkTransporte.OnUncheck.BorderThickness = 2;
            this.checkTransporte.OnUncheck.CheckBoxColor = System.Drawing.Color.Transparent;
            this.checkTransporte.Size = new System.Drawing.Size(25, 25);
            this.checkTransporte.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Round;
            this.checkTransporte.TabIndex = 12;
            this.checkTransporte.ThreeState = false;
            this.checkTransporte.ToolTipText = null;
            // 
            // txtResidencia
            // 
            this.txtResidencia.AcceptsReturn = false;
            this.txtResidencia.AcceptsTab = false;
            this.txtResidencia.AnimationSpeed = 200;
            this.txtResidencia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtResidencia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtResidencia.BackColor = System.Drawing.Color.White;
            this.txtResidencia.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtResidencia.BackgroundImage")));
            this.txtResidencia.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtResidencia.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtResidencia.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.txtResidencia.BorderColorIdle = System.Drawing.Color.Silver;
            this.txtResidencia.BorderRadius = 1;
            this.txtResidencia.BorderThickness = 1;
            this.txtResidencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtResidencia.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtResidencia.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F);
            this.txtResidencia.DefaultText = "";
            this.txtResidencia.FillColor = System.Drawing.Color.White;
            this.txtResidencia.HideSelection = true;
            this.txtResidencia.IconLeft = null;
            this.txtResidencia.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtResidencia.IconPadding = 10;
            this.txtResidencia.IconRight = null;
            this.txtResidencia.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtResidencia.Lines = new string[0];
            this.txtResidencia.Location = new System.Drawing.Point(69, 370);
            this.txtResidencia.MaxLength = 32767;
            this.txtResidencia.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtResidencia.Modified = false;
            this.txtResidencia.Multiline = false;
            this.txtResidencia.Name = "txtResidencia";
            stateProperties5.BorderColor = System.Drawing.Color.DodgerBlue;
            stateProperties5.FillColor = System.Drawing.Color.Empty;
            stateProperties5.ForeColor = System.Drawing.Color.Empty;
            stateProperties5.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtResidencia.OnActiveState = stateProperties5;
            stateProperties6.BorderColor = System.Drawing.Color.Empty;
            stateProperties6.FillColor = System.Drawing.Color.White;
            stateProperties6.ForeColor = System.Drawing.Color.Empty;
            stateProperties6.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtResidencia.OnDisabledState = stateProperties6;
            stateProperties7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties7.FillColor = System.Drawing.Color.Empty;
            stateProperties7.ForeColor = System.Drawing.Color.Empty;
            stateProperties7.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtResidencia.OnHoverState = stateProperties7;
            stateProperties8.BorderColor = System.Drawing.Color.Silver;
            stateProperties8.FillColor = System.Drawing.Color.White;
            stateProperties8.ForeColor = System.Drawing.Color.Empty;
            stateProperties8.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtResidencia.OnIdleState = stateProperties8;
            this.txtResidencia.PasswordChar = '\0';
            this.txtResidencia.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtResidencia.PlaceholderText = "Residencia";
            this.txtResidencia.ReadOnly = false;
            this.txtResidencia.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtResidencia.SelectedText = "";
            this.txtResidencia.SelectionLength = 0;
            this.txtResidencia.SelectionStart = 0;
            this.txtResidencia.ShortcutsEnabled = true;
            this.txtResidencia.Size = new System.Drawing.Size(387, 35);
            this.txtResidencia.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtResidencia.TabIndex = 10;
            this.txtResidencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtResidencia.TextMarginBottom = 0;
            this.txtResidencia.TextMarginLeft = 5;
            this.txtResidencia.TextMarginTop = 0;
            this.txtResidencia.TextPlaceholder = "Residencia";
            this.txtResidencia.UseSystemPasswordChar = false;
            this.txtResidencia.WordWrap = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.AllowToggling = false;
            this.btnGuardar.AnimationSpeed = 200;
            this.btnGuardar.AutoGenerateColors = false;
            this.btnGuardar.BackColor = System.Drawing.Color.Transparent;
            this.btnGuardar.BackColor1 = System.Drawing.Color.DodgerBlue;
            this.btnGuardar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardar.BackgroundImage")));
            this.btnGuardar.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btnGuardar.ButtonText = "Guardar";
            this.btnGuardar.ButtonTextMarginLeft = 0;
            this.btnGuardar.ColorContrastOnClick = 45;
            this.btnGuardar.ColorContrastOnHover = 45;
            this.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges3.BottomLeft = true;
            borderEdges3.BottomRight = true;
            borderEdges3.TopLeft = true;
            borderEdges3.TopRight = true;
            this.btnGuardar.CustomizableEdges = borderEdges3;
            this.btnGuardar.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnGuardar.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btnGuardar.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btnGuardar.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btnGuardar.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btnGuardar.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.Color.White;
            this.btnGuardar.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.IconMarginLeft = 11;
            this.btnGuardar.IconPadding = 10;
            this.btnGuardar.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.IdleBorderColor = System.Drawing.Color.DodgerBlue;
            this.btnGuardar.IdleBorderRadius = 7;
            this.btnGuardar.IdleBorderThickness = 1;
            this.btnGuardar.IdleFillColor = System.Drawing.Color.DodgerBlue;
            this.btnGuardar.IdleIconLeftImage = null;
            this.btnGuardar.IdleIconRightImage = null;
            this.btnGuardar.IndicateFocus = false;
            this.btnGuardar.Location = new System.Drawing.Point(189, 555);
            this.btnGuardar.Name = "btnGuardar";
            stateProperties9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties9.BorderRadius = 7;
            stateProperties9.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties9.BorderThickness = 1;
            stateProperties9.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties9.ForeColor = System.Drawing.Color.White;
            stateProperties9.IconLeftImage = null;
            stateProperties9.IconRightImage = null;
            this.btnGuardar.onHoverState = stateProperties9;
            stateProperties10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties10.BorderRadius = 7;
            stateProperties10.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            stateProperties10.BorderThickness = 1;
            stateProperties10.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            stateProperties10.ForeColor = System.Drawing.Color.White;
            stateProperties10.IconLeftImage = null;
            stateProperties10.IconRightImage = null;
            this.btnGuardar.OnPressedState = stateProperties10;
            this.btnGuardar.Size = new System.Drawing.Size(151, 45);
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnGuardar.TextMarginLeft = 0;
            this.btnGuardar.UseDefaultRadiusAndThickness = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtApellidos
            // 
            this.txtApellidos.AcceptsReturn = false;
            this.txtApellidos.AcceptsTab = false;
            this.txtApellidos.AnimationSpeed = 200;
            this.txtApellidos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtApellidos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtApellidos.BackColor = System.Drawing.Color.White;
            this.txtApellidos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtApellidos.BackgroundImage")));
            this.txtApellidos.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtApellidos.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtApellidos.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.txtApellidos.BorderColorIdle = System.Drawing.Color.Silver;
            this.txtApellidos.BorderRadius = 1;
            this.txtApellidos.BorderThickness = 1;
            this.txtApellidos.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtApellidos.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtApellidos.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidos.DefaultText = "";
            this.txtApellidos.FillColor = System.Drawing.Color.White;
            this.txtApellidos.HideSelection = true;
            this.txtApellidos.IconLeft = null;
            this.txtApellidos.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtApellidos.IconPadding = 10;
            this.txtApellidos.IconRight = null;
            this.txtApellidos.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtApellidos.Lines = new string[0];
            this.txtApellidos.Location = new System.Drawing.Point(69, 152);
            this.txtApellidos.MaxLength = 32767;
            this.txtApellidos.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtApellidos.Modified = false;
            this.txtApellidos.Multiline = false;
            this.txtApellidos.Name = "txtApellidos";
            stateProperties11.BorderColor = System.Drawing.Color.DodgerBlue;
            stateProperties11.FillColor = System.Drawing.Color.Empty;
            stateProperties11.ForeColor = System.Drawing.Color.Empty;
            stateProperties11.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtApellidos.OnActiveState = stateProperties11;
            stateProperties12.BorderColor = System.Drawing.Color.Empty;
            stateProperties12.FillColor = System.Drawing.Color.White;
            stateProperties12.ForeColor = System.Drawing.Color.Empty;
            stateProperties12.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtApellidos.OnDisabledState = stateProperties12;
            stateProperties13.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties13.FillColor = System.Drawing.Color.Empty;
            stateProperties13.ForeColor = System.Drawing.Color.Empty;
            stateProperties13.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtApellidos.OnHoverState = stateProperties13;
            stateProperties14.BorderColor = System.Drawing.Color.Silver;
            stateProperties14.FillColor = System.Drawing.Color.White;
            stateProperties14.ForeColor = System.Drawing.Color.Empty;
            stateProperties14.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtApellidos.OnIdleState = stateProperties14;
            this.txtApellidos.PasswordChar = '\0';
            this.txtApellidos.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtApellidos.PlaceholderText = "Apellidos";
            this.txtApellidos.ReadOnly = false;
            this.txtApellidos.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtApellidos.SelectedText = "";
            this.txtApellidos.SelectionLength = 0;
            this.txtApellidos.SelectionStart = 0;
            this.txtApellidos.ShortcutsEnabled = true;
            this.txtApellidos.Size = new System.Drawing.Size(387, 35);
            this.txtApellidos.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtApellidos.TabIndex = 6;
            this.txtApellidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtApellidos.TextMarginBottom = 0;
            this.txtApellidos.TextMarginLeft = 5;
            this.txtApellidos.TextMarginTop = 0;
            this.txtApellidos.TextPlaceholder = "Apellidos";
            this.txtApellidos.UseSystemPasswordChar = false;
            this.txtApellidos.WordWrap = true;
            // 
            // txtCedula
            // 
            this.txtCedula.AcceptsReturn = false;
            this.txtCedula.AcceptsTab = false;
            this.txtCedula.AnimationSpeed = 200;
            this.txtCedula.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCedula.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCedula.BackColor = System.Drawing.Color.White;
            this.txtCedula.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCedula.BackgroundImage")));
            this.txtCedula.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCedula.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCedula.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.txtCedula.BorderColorIdle = System.Drawing.Color.Silver;
            this.txtCedula.BorderRadius = 1;
            this.txtCedula.BorderThickness = 1;
            this.txtCedula.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCedula.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCedula.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCedula.DefaultText = "";
            this.txtCedula.FillColor = System.Drawing.Color.White;
            this.txtCedula.HideSelection = true;
            this.txtCedula.IconLeft = null;
            this.txtCedula.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCedula.IconPadding = 10;
            this.txtCedula.IconRight = null;
            this.txtCedula.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCedula.Lines = new string[0];
            this.txtCedula.Location = new System.Drawing.Point(69, 224);
            this.txtCedula.MaxLength = 32767;
            this.txtCedula.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtCedula.Modified = false;
            this.txtCedula.Multiline = false;
            this.txtCedula.Name = "txtCedula";
            stateProperties15.BorderColor = System.Drawing.Color.DodgerBlue;
            stateProperties15.FillColor = System.Drawing.Color.Empty;
            stateProperties15.ForeColor = System.Drawing.Color.Empty;
            stateProperties15.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtCedula.OnActiveState = stateProperties15;
            stateProperties16.BorderColor = System.Drawing.Color.Empty;
            stateProperties16.FillColor = System.Drawing.Color.White;
            stateProperties16.ForeColor = System.Drawing.Color.Empty;
            stateProperties16.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtCedula.OnDisabledState = stateProperties16;
            stateProperties17.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties17.FillColor = System.Drawing.Color.Empty;
            stateProperties17.ForeColor = System.Drawing.Color.Empty;
            stateProperties17.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtCedula.OnHoverState = stateProperties17;
            stateProperties18.BorderColor = System.Drawing.Color.Silver;
            stateProperties18.FillColor = System.Drawing.Color.White;
            stateProperties18.ForeColor = System.Drawing.Color.Empty;
            stateProperties18.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtCedula.OnIdleState = stateProperties18;
            this.txtCedula.PasswordChar = '\0';
            this.txtCedula.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtCedula.PlaceholderText = "Cédula";
            this.txtCedula.ReadOnly = false;
            this.txtCedula.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCedula.SelectedText = "";
            this.txtCedula.SelectionLength = 0;
            this.txtCedula.SelectionStart = 0;
            this.txtCedula.ShortcutsEnabled = true;
            this.txtCedula.Size = new System.Drawing.Size(387, 35);
            this.txtCedula.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCedula.TabIndex = 5;
            this.txtCedula.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCedula.TextMarginBottom = 0;
            this.txtCedula.TextMarginLeft = 5;
            this.txtCedula.TextMarginTop = 0;
            this.txtCedula.TextPlaceholder = "Cédula";
            this.txtCedula.UseSystemPasswordChar = false;
            this.txtCedula.WordWrap = true;
            // 
            // txtEncargado
            // 
            this.txtEncargado.AcceptsReturn = false;
            this.txtEncargado.AcceptsTab = false;
            this.txtEncargado.AnimationSpeed = 200;
            this.txtEncargado.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtEncargado.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtEncargado.BackColor = System.Drawing.Color.White;
            this.txtEncargado.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtEncargado.BackgroundImage")));
            this.txtEncargado.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtEncargado.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtEncargado.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.txtEncargado.BorderColorIdle = System.Drawing.Color.Silver;
            this.txtEncargado.BorderRadius = 1;
            this.txtEncargado.BorderThickness = 1;
            this.txtEncargado.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtEncargado.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEncargado.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F);
            this.txtEncargado.DefaultText = "";
            this.txtEncargado.FillColor = System.Drawing.Color.White;
            this.txtEncargado.HideSelection = true;
            this.txtEncargado.IconLeft = null;
            this.txtEncargado.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEncargado.IconPadding = 10;
            this.txtEncargado.IconRight = null;
            this.txtEncargado.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEncargado.Lines = new string[0];
            this.txtEncargado.Location = new System.Drawing.Point(69, 296);
            this.txtEncargado.MaxLength = 32767;
            this.txtEncargado.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtEncargado.Modified = false;
            this.txtEncargado.Multiline = false;
            this.txtEncargado.Name = "txtEncargado";
            stateProperties19.BorderColor = System.Drawing.Color.DodgerBlue;
            stateProperties19.FillColor = System.Drawing.Color.Empty;
            stateProperties19.ForeColor = System.Drawing.Color.Empty;
            stateProperties19.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtEncargado.OnActiveState = stateProperties19;
            stateProperties20.BorderColor = System.Drawing.Color.Empty;
            stateProperties20.FillColor = System.Drawing.Color.White;
            stateProperties20.ForeColor = System.Drawing.Color.Empty;
            stateProperties20.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtEncargado.OnDisabledState = stateProperties20;
            stateProperties21.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties21.FillColor = System.Drawing.Color.Empty;
            stateProperties21.ForeColor = System.Drawing.Color.Empty;
            stateProperties21.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtEncargado.OnHoverState = stateProperties21;
            stateProperties22.BorderColor = System.Drawing.Color.Silver;
            stateProperties22.FillColor = System.Drawing.Color.White;
            stateProperties22.ForeColor = System.Drawing.Color.Empty;
            stateProperties22.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtEncargado.OnIdleState = stateProperties22;
            this.txtEncargado.PasswordChar = '\0';
            this.txtEncargado.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtEncargado.PlaceholderText = "Numero de encargado";
            this.txtEncargado.ReadOnly = false;
            this.txtEncargado.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEncargado.SelectedText = "";
            this.txtEncargado.SelectionLength = 0;
            this.txtEncargado.SelectionStart = 0;
            this.txtEncargado.ShortcutsEnabled = true;
            this.txtEncargado.Size = new System.Drawing.Size(387, 35);
            this.txtEncargado.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtEncargado.TabIndex = 2;
            this.txtEncargado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtEncargado.TextMarginBottom = 0;
            this.txtEncargado.TextMarginLeft = 5;
            this.txtEncargado.TextMarginTop = 0;
            this.txtEncargado.TextPlaceholder = "Numero de encargado";
            this.txtEncargado.UseSystemPasswordChar = false;
            this.txtEncargado.WordWrap = true;
            // 
            // txtNombre
            // 
            this.txtNombre.AcceptsReturn = false;
            this.txtNombre.AcceptsTab = false;
            this.txtNombre.AnimationSpeed = 200;
            this.txtNombre.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtNombre.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtNombre.BackColor = System.Drawing.Color.White;
            this.txtNombre.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNombre.BackgroundImage")));
            this.txtNombre.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtNombre.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtNombre.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.txtNombre.BorderColorIdle = System.Drawing.Color.Silver;
            this.txtNombre.BorderRadius = 1;
            this.txtNombre.BorderThickness = 1;
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNombre.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F);
            this.txtNombre.DefaultText = "";
            this.txtNombre.FillColor = System.Drawing.Color.White;
            this.txtNombre.HideSelection = true;
            this.txtNombre.IconLeft = null;
            this.txtNombre.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNombre.IconPadding = 10;
            this.txtNombre.IconRight = null;
            this.txtNombre.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNombre.Lines = new string[0];
            this.txtNombre.Location = new System.Drawing.Point(69, 85);
            this.txtNombre.MaxLength = 32767;
            this.txtNombre.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtNombre.Modified = false;
            this.txtNombre.Multiline = false;
            this.txtNombre.Name = "txtNombre";
            stateProperties23.BorderColor = System.Drawing.Color.DodgerBlue;
            stateProperties23.FillColor = System.Drawing.Color.Empty;
            stateProperties23.ForeColor = System.Drawing.Color.Empty;
            stateProperties23.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtNombre.OnActiveState = stateProperties23;
            stateProperties24.BorderColor = System.Drawing.Color.Empty;
            stateProperties24.FillColor = System.Drawing.Color.White;
            stateProperties24.ForeColor = System.Drawing.Color.Empty;
            stateProperties24.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtNombre.OnDisabledState = stateProperties24;
            stateProperties25.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties25.FillColor = System.Drawing.Color.Empty;
            stateProperties25.ForeColor = System.Drawing.Color.Empty;
            stateProperties25.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtNombre.OnHoverState = stateProperties25;
            stateProperties26.BorderColor = System.Drawing.Color.Silver;
            stateProperties26.FillColor = System.Drawing.Color.White;
            stateProperties26.ForeColor = System.Drawing.Color.Empty;
            stateProperties26.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtNombre.OnIdleState = stateProperties26;
            this.txtNombre.PasswordChar = '\0';
            this.txtNombre.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtNombre.PlaceholderText = "Nombre";
            this.txtNombre.ReadOnly = false;
            this.txtNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNombre.SelectedText = "";
            this.txtNombre.SelectionLength = 0;
            this.txtNombre.SelectionStart = 0;
            this.txtNombre.ShortcutsEnabled = true;
            this.txtNombre.Size = new System.Drawing.Size(387, 35);
            this.txtNombre.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtNombre.TabIndex = 1;
            this.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtNombre.TextMarginBottom = 0;
            this.txtNombre.TextMarginLeft = 5;
            this.txtNombre.TextMarginTop = 0;
            this.txtNombre.TextPlaceholder = "Nombre";
            this.txtNombre.UseSystemPasswordChar = false;
            this.txtNombre.WordWrap = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(109, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(347, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "Información del estudiante";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeight = 45;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.nombre,
            this.apellidos,
            this.cedula,
            this.numero,
            this.residencia,
            this.becaTransporte,
            this.becaComedor});
            this.dgv.DoubleBuffered = true;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.GridColor = System.Drawing.Color.White;
            this.dgv.HeaderBgColor = System.Drawing.Color.DodgerBlue;
            this.dgv.HeaderForeColor = System.Drawing.Color.White;
            this.dgv.Location = new System.Drawing.Point(37, 113);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersWidth = 51;
            this.dgv.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            this.dgv.RowTemplate.Height = 24;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(927, 627);
            this.dgv.TabIndex = 4;
            this.dgv.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_CellMouseClick);
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 125;
            // 
            // nombre
            // 
            this.nombre.HeaderText = "Nombre";
            this.nombre.MinimumWidth = 6;
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            this.nombre.Width = 125;
            // 
            // apellidos
            // 
            this.apellidos.HeaderText = "Apellidos";
            this.apellidos.MinimumWidth = 6;
            this.apellidos.Name = "apellidos";
            this.apellidos.ReadOnly = true;
            this.apellidos.Width = 125;
            // 
            // cedula
            // 
            this.cedula.HeaderText = "Cédula";
            this.cedula.MinimumWidth = 6;
            this.cedula.Name = "cedula";
            this.cedula.ReadOnly = true;
            this.cedula.Width = 125;
            // 
            // numero
            // 
            this.numero.HeaderText = "Número encargado";
            this.numero.MinimumWidth = 6;
            this.numero.Name = "numero";
            this.numero.ReadOnly = true;
            this.numero.Width = 125;
            // 
            // residencia
            // 
            this.residencia.HeaderText = "Residencia";
            this.residencia.MinimumWidth = 6;
            this.residencia.Name = "residencia";
            this.residencia.ReadOnly = true;
            this.residencia.Width = 125;
            // 
            // becaTransporte
            // 
            this.becaTransporte.HeaderText = "Beca transporte";
            this.becaTransporte.MinimumWidth = 6;
            this.becaTransporte.Name = "becaTransporte";
            this.becaTransporte.ReadOnly = true;
            this.becaTransporte.Width = 125;
            // 
            // becaComedor
            // 
            this.becaComedor.HeaderText = "Beca comedor";
            this.becaComedor.MinimumWidth = 6;
            this.becaComedor.Name = "becaComedor";
            this.becaComedor.ReadOnly = true;
            this.becaComedor.Width = 125;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.bunifuImageButton2);
            this.panel1.Controls.Add(this.bunifuImageButton3);
            this.panel1.Controls.Add(this.txtBuscar);
            this.panel1.Controls.Add(this.bunifuImageButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1556, 72);
            this.panel1.TabIndex = 7;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(1458, 12);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(59, 42);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 10;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 10;
            this.bunifuImageButton2.Click += new System.EventHandler(this.bunifuImageButton2_Click);
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.Image")));
            this.bunifuImageButton3.ImageActive = null;
            this.bunifuImageButton3.Location = new System.Drawing.Point(1393, 12);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(70, 57);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton3.TabIndex = 5;
            this.bunifuImageButton3.TabStop = false;
            this.bunifuImageButton3.Zoom = 10;
            this.bunifuImageButton3.Click += new System.EventHandler(this.bunifuImageButton3_Click);
            // 
            // txtBuscar
            // 
            this.txtBuscar.AcceptsReturn = false;
            this.txtBuscar.AcceptsTab = false;
            this.txtBuscar.AnimationSpeed = 200;
            this.txtBuscar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtBuscar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtBuscar.BackColor = System.Drawing.Color.Transparent;
            this.txtBuscar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtBuscar.BackgroundImage")));
            this.txtBuscar.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtBuscar.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtBuscar.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.txtBuscar.BorderColorIdle = System.Drawing.Color.Transparent;
            this.txtBuscar.BorderRadius = 30;
            this.txtBuscar.BorderThickness = 1;
            this.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtBuscar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBuscar.DefaultFont = new System.Drawing.Font("Century Gothic", 10.2F);
            this.txtBuscar.DefaultText = "";
            this.txtBuscar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(249)))));
            this.txtBuscar.HideSelection = true;
            this.txtBuscar.IconLeft = null;
            this.txtBuscar.IconLeftCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBuscar.IconPadding = 10;
            this.txtBuscar.IconRight = ((System.Drawing.Image)(resources.GetObject("txtBuscar.IconRight")));
            this.txtBuscar.IconRightCursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBuscar.Lines = new string[0];
            this.txtBuscar.Location = new System.Drawing.Point(111, 12);
            this.txtBuscar.MaxLength = 32767;
            this.txtBuscar.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtBuscar.Modified = false;
            this.txtBuscar.Multiline = false;
            this.txtBuscar.Name = "txtBuscar";
            stateProperties27.BorderColor = System.Drawing.Color.DodgerBlue;
            stateProperties27.FillColor = System.Drawing.Color.Empty;
            stateProperties27.ForeColor = System.Drawing.Color.Empty;
            stateProperties27.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtBuscar.OnActiveState = stateProperties27;
            stateProperties28.BorderColor = System.Drawing.Color.Empty;
            stateProperties28.FillColor = System.Drawing.Color.White;
            stateProperties28.ForeColor = System.Drawing.Color.Empty;
            stateProperties28.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txtBuscar.OnDisabledState = stateProperties28;
            stateProperties29.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            stateProperties29.FillColor = System.Drawing.Color.Empty;
            stateProperties29.ForeColor = System.Drawing.Color.Empty;
            stateProperties29.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtBuscar.OnHoverState = stateProperties29;
            stateProperties30.BorderColor = System.Drawing.Color.Transparent;
            stateProperties30.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(249)))));
            stateProperties30.ForeColor = System.Drawing.Color.Empty;
            stateProperties30.PlaceholderForeColor = System.Drawing.Color.Empty;
            this.txtBuscar.OnIdleState = stateProperties30;
            this.txtBuscar.PasswordChar = '\0';
            this.txtBuscar.PlaceholderForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.txtBuscar.PlaceholderText = "Buscar estudiante";
            this.txtBuscar.ReadOnly = false;
            this.txtBuscar.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBuscar.SelectedText = "";
            this.txtBuscar.SelectionLength = 0;
            this.txtBuscar.SelectionStart = 0;
            this.txtBuscar.ShortcutsEnabled = true;
            this.txtBuscar.Size = new System.Drawing.Size(348, 42);
            this.txtBuscar.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtBuscar.TabIndex = 4;
            this.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtBuscar.TextMarginBottom = 0;
            this.txtBuscar.TextMarginLeft = 5;
            this.txtBuscar.TextMarginTop = 0;
            this.txtBuscar.TextPlaceholder = "Buscar estudiante";
            this.txtBuscar.UseSystemPasswordChar = false;
            this.txtBuscar.WordWrap = true;
            this.txtBuscar.TextChange += new System.EventHandler(this.bunifuTextBox3_TextChange);
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(0, 3);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(59, 66);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 3;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // MenuEstudiante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(249)))));
            this.ClientSize = new System.Drawing.Size(1556, 808);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dgv);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuEstudiante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuEstudiante";
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnGuardar;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtApellidos;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCedula;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtEncargado;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dgv;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtBuscar;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.UI.WinForms.BunifuCheckBox checkTransporte;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtResidencia;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.UI.WinForms.BunifuCheckBox checkComedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn residencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn becaTransporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn becaComedor;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnNuevo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnEliminar;
    }
}