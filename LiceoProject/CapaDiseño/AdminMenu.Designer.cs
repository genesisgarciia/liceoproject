﻿namespace CapaDiseño
{
    partial class AdminMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.LinkLabel linkLabel1;
            System.Windows.Forms.LinkLabel linkLabel2;
            System.Windows.Forms.LinkLabel linkLabel3;
            System.Windows.Forms.LinkLabel linkLabel4;
            System.Windows.Forms.LinkLabel linkLabel5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminMenu));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bunifuImageButton4 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton5 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuPictureBox6 = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.bunifuPictureBox5 = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.bunifuPictureBox3 = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.bunifuPictureBox2 = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.bunifuPictureBox1 = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.bunifuUserControl6 = new Bunifu.UI.WinForms.BunifuUserControl();
            this.bunifuUserControl8 = new Bunifu.UI.WinForms.BunifuUserControl();
            this.bunifuUserControl4 = new Bunifu.UI.WinForms.BunifuUserControl();
            this.bunifuUserControl3 = new Bunifu.UI.WinForms.BunifuUserControl();
            this.bunifuUserControl1 = new Bunifu.UI.WinForms.BunifuUserControl();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            linkLabel1 = new System.Windows.Forms.LinkLabel();
            linkLabel2 = new System.Windows.Forms.LinkLabel();
            linkLabel3 = new System.Windows.Forms.LinkLabel();
            linkLabel4 = new System.Windows.Forms.LinkLabel();
            linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // linkLabel1
            // 
            linkLabel1.ActiveLinkColor = System.Drawing.Color.Black;
            linkLabel1.BackColor = System.Drawing.Color.LightBlue;
            linkLabel1.DisabledLinkColor = System.Drawing.Color.DeepSkyBlue;
            linkLabel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            linkLabel1.LinkColor = System.Drawing.Color.SlateGray;
            linkLabel1.Location = new System.Drawing.Point(71, 269);
            linkLabel1.Name = "linkLabel1";
            linkLabel1.Size = new System.Drawing.Size(228, 42);
            linkLabel1.TabIndex = 17;
            linkLabel1.TabStop = true;
            linkLabel1.Text = "TRANSPORTE";
            linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            linkLabel2.ActiveLinkColor = System.Drawing.Color.Black;
            linkLabel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(240)))), ((int)(((byte)(207)))));
            linkLabel2.DisabledLinkColor = System.Drawing.Color.DeepSkyBlue;
            linkLabel2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            linkLabel2.LinkColor = System.Drawing.Color.SlateGray;
            linkLabel2.Location = new System.Drawing.Point(71, 573);
            linkLabel2.Name = "linkLabel2";
            linkLabel2.Size = new System.Drawing.Size(228, 42);
            linkLabel2.TabIndex = 18;
            linkLabel2.TabStop = true;
            linkLabel2.Text = "ESTUDIANTES";
            linkLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel3
            // 
            linkLabel3.ActiveLinkColor = System.Drawing.Color.Black;
            linkLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(223)))), ((int)(((byte)(249)))));
            linkLabel3.DisabledLinkColor = System.Drawing.Color.DeepSkyBlue;
            linkLabel3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            linkLabel3.LinkColor = System.Drawing.Color.SlateGray;
            linkLabel3.Location = new System.Drawing.Point(667, 269);
            linkLabel3.Name = "linkLabel3";
            linkLabel3.Size = new System.Drawing.Size(228, 69);
            linkLabel3.TabIndex = 19;
            linkLabel3.TabStop = true;
            linkLabel3.Text = "ADMINISTRACIÓN DE USUARIOS";
            linkLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // linkLabel4
            // 
            linkLabel4.ActiveLinkColor = System.Drawing.Color.Black;
            linkLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(201)))), ((int)(((byte)(149)))));
            linkLabel4.DisabledLinkColor = System.Drawing.Color.DeepSkyBlue;
            linkLabel4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            linkLabel4.LinkColor = System.Drawing.Color.SlateGray;
            linkLabel4.Location = new System.Drawing.Point(372, 269);
            linkLabel4.Name = "linkLabel4";
            linkLabel4.Size = new System.Drawing.Size(228, 42);
            linkLabel4.TabIndex = 20;
            linkLabel4.TabStop = true;
            linkLabel4.Text = "COMEDOR";
            linkLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // linkLabel5
            // 
            linkLabel5.ActiveLinkColor = System.Drawing.Color.Black;
            linkLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(235)))), ((int)(((byte)(236)))));
            linkLabel5.DisabledLinkColor = System.Drawing.Color.DeepSkyBlue;
            linkLabel5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            linkLabel5.LinkColor = System.Drawing.Color.SlateGray;
            linkLabel5.Location = new System.Drawing.Point(372, 573);
            linkLabel5.Name = "linkLabel5";
            linkLabel5.Size = new System.Drawing.Size(228, 42);
            linkLabel5.TabIndex = 21;
            linkLabel5.TabStop = true;
            linkLabel5.Text = "REPORTES";
            linkLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(linkLabel5);
            this.panel1.Controls.Add(linkLabel4);
            this.panel1.Controls.Add(linkLabel3);
            this.panel1.Controls.Add(linkLabel2);
            this.panel1.Controls.Add(linkLabel1);
            this.panel1.Controls.Add(this.bunifuPictureBox6);
            this.panel1.Controls.Add(this.bunifuPictureBox5);
            this.panel1.Controls.Add(this.bunifuPictureBox3);
            this.panel1.Controls.Add(this.bunifuPictureBox2);
            this.panel1.Controls.Add(this.bunifuPictureBox1);
            this.panel1.Controls.Add(this.bunifuUserControl6);
            this.panel1.Controls.Add(this.bunifuUserControl8);
            this.panel1.Controls.Add(this.bunifuUserControl4);
            this.panel1.Controls.Add(this.bunifuUserControl3);
            this.panel1.Controls.Add(this.bunifuUserControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(962, 714);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(249)))));
            this.panel2.Controls.Add(this.bunifuImageButton4);
            this.panel2.Controls.Add(this.bunifuImageButton5);
            this.panel2.Controls.Add(this.bunifuImageButton2);
            this.panel2.Controls.Add(this.bunifuImageButton3);
            this.panel2.Controls.Add(this.bunifuImageButton1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(962, 61);
            this.panel2.TabIndex = 23;
            // 
            // bunifuImageButton4
            // 
            this.bunifuImageButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton4.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton4.Image")));
            this.bunifuImageButton4.ImageActive = null;
            this.bunifuImageButton4.Location = new System.Drawing.Point(882, 4);
            this.bunifuImageButton4.Name = "bunifuImageButton4";
            this.bunifuImageButton4.Size = new System.Drawing.Size(59, 42);
            this.bunifuImageButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton4.TabIndex = 12;
            this.bunifuImageButton4.TabStop = false;
            this.bunifuImageButton4.Zoom = 10;
            this.bunifuImageButton4.Click += new System.EventHandler(this.bunifuImageButton4_Click);
            // 
            // bunifuImageButton5
            // 
            this.bunifuImageButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton5.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton5.Image")));
            this.bunifuImageButton5.ImageActive = null;
            this.bunifuImageButton5.Location = new System.Drawing.Point(817, 4);
            this.bunifuImageButton5.Name = "bunifuImageButton5";
            this.bunifuImageButton5.Size = new System.Drawing.Size(70, 57);
            this.bunifuImageButton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton5.TabIndex = 11;
            this.bunifuImageButton5.TabStop = false;
            this.bunifuImageButton5.Zoom = 10;
            this.bunifuImageButton5.Click += new System.EventHandler(this.bunifuImageButton5_Click);
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(1458, 12);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(59, 42);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 10;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.Zoom = 10;
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.Image")));
            this.bunifuImageButton3.ImageActive = null;
            this.bunifuImageButton3.Location = new System.Drawing.Point(1393, 12);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(70, 57);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton3.TabIndex = 5;
            this.bunifuImageButton3.TabStop = false;
            this.bunifuImageButton3.Zoom = 10;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(0, 3);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(59, 43);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 3;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // bunifuPictureBox6
            // 
            this.bunifuPictureBox6.AllowFocused = false;
            this.bunifuPictureBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuPictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuPictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bunifuPictureBox6.BorderRadius = 50;
            this.bunifuPictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("bunifuPictureBox6.Image")));
            this.bunifuPictureBox6.IsCircle = false;
            this.bunifuPictureBox6.Location = new System.Drawing.Point(727, 130);
            this.bunifuPictureBox6.Name = "bunifuPictureBox6";
            this.bunifuPictureBox6.Size = new System.Drawing.Size(127, 127);
            this.bunifuPictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuPictureBox6.TabIndex = 16;
            this.bunifuPictureBox6.TabStop = false;
            this.bunifuPictureBox6.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            this.bunifuPictureBox6.Click += new System.EventHandler(this.bunifuPictureBox6_Click);
            // 
            // bunifuPictureBox5
            // 
            this.bunifuPictureBox5.AllowFocused = false;
            this.bunifuPictureBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuPictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuPictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bunifuPictureBox5.BorderRadius = 50;
            this.bunifuPictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("bunifuPictureBox5.Image")));
            this.bunifuPictureBox5.IsCircle = false;
            this.bunifuPictureBox5.Location = new System.Drawing.Point(121, 428);
            this.bunifuPictureBox5.Name = "bunifuPictureBox5";
            this.bunifuPictureBox5.Size = new System.Drawing.Size(127, 127);
            this.bunifuPictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuPictureBox5.TabIndex = 15;
            this.bunifuPictureBox5.TabStop = false;
            this.bunifuPictureBox5.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            this.bunifuPictureBox5.Click += new System.EventHandler(this.bunifuPictureBox5_Click);
            // 
            // bunifuPictureBox3
            // 
            this.bunifuPictureBox3.AllowFocused = false;
            this.bunifuPictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuPictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuPictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bunifuPictureBox3.BorderRadius = 50;
            this.bunifuPictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuPictureBox3.Image")));
            this.bunifuPictureBox3.IsCircle = false;
            this.bunifuPictureBox3.Location = new System.Drawing.Point(427, 428);
            this.bunifuPictureBox3.Name = "bunifuPictureBox3";
            this.bunifuPictureBox3.Size = new System.Drawing.Size(127, 127);
            this.bunifuPictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuPictureBox3.TabIndex = 13;
            this.bunifuPictureBox3.TabStop = false;
            this.bunifuPictureBox3.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            this.bunifuPictureBox3.Click += new System.EventHandler(this.bunifuPictureBox3_Click);
            // 
            // bunifuPictureBox2
            // 
            this.bunifuPictureBox2.AllowFocused = false;
            this.bunifuPictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuPictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuPictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bunifuPictureBox2.BorderRadius = 50;
            this.bunifuPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuPictureBox2.Image")));
            this.bunifuPictureBox2.IsCircle = false;
            this.bunifuPictureBox2.Location = new System.Drawing.Point(121, 130);
            this.bunifuPictureBox2.Name = "bunifuPictureBox2";
            this.bunifuPictureBox2.Size = new System.Drawing.Size(127, 127);
            this.bunifuPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuPictureBox2.TabIndex = 12;
            this.bunifuPictureBox2.TabStop = false;
            this.bunifuPictureBox2.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            this.bunifuPictureBox2.Click += new System.EventHandler(this.bunifuPictureBox2_Click);
            // 
            // bunifuPictureBox1
            // 
            this.bunifuPictureBox1.AllowFocused = false;
            this.bunifuPictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuPictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bunifuPictureBox1.BorderRadius = 50;
            this.bunifuPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuPictureBox1.Image")));
            this.bunifuPictureBox1.IsCircle = false;
            this.bunifuPictureBox1.Location = new System.Drawing.Point(427, 130);
            this.bunifuPictureBox1.Name = "bunifuPictureBox1";
            this.bunifuPictureBox1.Size = new System.Drawing.Size(127, 127);
            this.bunifuPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuPictureBox1.TabIndex = 11;
            this.bunifuPictureBox1.TabStop = false;
            this.bunifuPictureBox1.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            this.bunifuPictureBox1.Click += new System.EventHandler(this.bunifuPictureBox1_Click);
            // 
            // bunifuUserControl6
            // 
            this.bunifuUserControl6.AllowAnimations = false;
            this.bunifuUserControl6.AllowBorderColorChanges = false;
            this.bunifuUserControl6.AllowMouseEffects = false;
            this.bunifuUserControl6.AnimationSpeed = 200;
            this.bunifuUserControl6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuUserControl6.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(235)))), ((int)(((byte)(236)))));
            this.bunifuUserControl6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(69)))), ((int)(((byte)(155)))));
            this.bunifuUserControl6.BorderRadius = 20;
            this.bunifuUserControl6.BorderThickness = 2;
            this.bunifuUserControl6.ColorContrastOnClick = 30;
            this.bunifuUserControl6.ColorContrastOnHover = 30;
            this.bunifuUserControl6.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuUserControl6.Image = null;
            this.bunifuUserControl6.ImageMargin = new System.Windows.Forms.Padding(0);
            this.bunifuUserControl6.Location = new System.Drawing.Point(361, 500);
            this.bunifuUserControl6.Name = "bunifuUserControl6";
            this.bunifuUserControl6.ShowBorders = false;
            this.bunifuUserControl6.Size = new System.Drawing.Size(248, 163);
            this.bunifuUserControl6.Style = Bunifu.UI.WinForms.BunifuUserControl.UserControlStyles.Flat;
            this.bunifuUserControl6.TabIndex = 10;
            this.bunifuUserControl6.Click += new System.EventHandler(this.bunifuUserControl6_Click);
            // 
            // bunifuUserControl8
            // 
            this.bunifuUserControl8.AllowAnimations = false;
            this.bunifuUserControl8.AllowBorderColorChanges = false;
            this.bunifuUserControl8.AllowMouseEffects = false;
            this.bunifuUserControl8.AnimationSpeed = 200;
            this.bunifuUserControl8.BackColor = System.Drawing.Color.Transparent;
            this.bunifuUserControl8.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(223)))), ((int)(((byte)(249)))));
            this.bunifuUserControl8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(69)))), ((int)(((byte)(155)))));
            this.bunifuUserControl8.BorderRadius = 20;
            this.bunifuUserControl8.BorderThickness = 2;
            this.bunifuUserControl8.ColorContrastOnClick = 30;
            this.bunifuUserControl8.ColorContrastOnHover = 30;
            this.bunifuUserControl8.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuUserControl8.Image = null;
            this.bunifuUserControl8.ImageMargin = new System.Windows.Forms.Padding(0);
            this.bunifuUserControl8.Location = new System.Drawing.Point(657, 200);
            this.bunifuUserControl8.Name = "bunifuUserControl8";
            this.bunifuUserControl8.ShowBorders = false;
            this.bunifuUserControl8.Size = new System.Drawing.Size(248, 163);
            this.bunifuUserControl8.Style = Bunifu.UI.WinForms.BunifuUserControl.UserControlStyles.Flat;
            this.bunifuUserControl8.TabIndex = 8;
            this.bunifuUserControl8.Click += new System.EventHandler(this.bunifuUserControl8_Click);
            // 
            // bunifuUserControl4
            // 
            this.bunifuUserControl4.AllowAnimations = false;
            this.bunifuUserControl4.AllowBorderColorChanges = false;
            this.bunifuUserControl4.AllowMouseEffects = false;
            this.bunifuUserControl4.AnimationSpeed = 200;
            this.bunifuUserControl4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuUserControl4.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(201)))), ((int)(((byte)(149)))));
            this.bunifuUserControl4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(69)))), ((int)(((byte)(155)))));
            this.bunifuUserControl4.BorderRadius = 20;
            this.bunifuUserControl4.BorderThickness = 0;
            this.bunifuUserControl4.ColorContrastOnClick = 30;
            this.bunifuUserControl4.ColorContrastOnHover = 30;
            this.bunifuUserControl4.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuUserControl4.Image = null;
            this.bunifuUserControl4.ImageMargin = new System.Windows.Forms.Padding(0);
            this.bunifuUserControl4.Location = new System.Drawing.Point(361, 200);
            this.bunifuUserControl4.Name = "bunifuUserControl4";
            this.bunifuUserControl4.ShowBorders = false;
            this.bunifuUserControl4.Size = new System.Drawing.Size(248, 163);
            this.bunifuUserControl4.Style = Bunifu.UI.WinForms.BunifuUserControl.UserControlStyles.Flat;
            this.bunifuUserControl4.TabIndex = 7;
            this.bunifuUserControl4.Click += new System.EventHandler(this.bunifuUserControl4_Click);
            // 
            // bunifuUserControl3
            // 
            this.bunifuUserControl3.AllowAnimations = false;
            this.bunifuUserControl3.AllowBorderColorChanges = false;
            this.bunifuUserControl3.AllowMouseEffects = false;
            this.bunifuUserControl3.AnimationSpeed = 200;
            this.bunifuUserControl3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuUserControl3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(240)))), ((int)(((byte)(207)))));
            this.bunifuUserControl3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(69)))), ((int)(((byte)(155)))));
            this.bunifuUserControl3.BorderRadius = 20;
            this.bunifuUserControl3.BorderThickness = 2;
            this.bunifuUserControl3.ColorContrastOnClick = 30;
            this.bunifuUserControl3.ColorContrastOnHover = 30;
            this.bunifuUserControl3.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuUserControl3.Image = null;
            this.bunifuUserControl3.ImageMargin = new System.Windows.Forms.Padding(0);
            this.bunifuUserControl3.Location = new System.Drawing.Point(61, 500);
            this.bunifuUserControl3.Name = "bunifuUserControl3";
            this.bunifuUserControl3.ShowBorders = false;
            this.bunifuUserControl3.Size = new System.Drawing.Size(248, 163);
            this.bunifuUserControl3.Style = Bunifu.UI.WinForms.BunifuUserControl.UserControlStyles.Flat;
            this.bunifuUserControl3.TabIndex = 6;
            this.bunifuUserControl3.Click += new System.EventHandler(this.bunifuUserControl3_Click);
            // 
            // bunifuUserControl1
            // 
            this.bunifuUserControl1.AllowAnimations = false;
            this.bunifuUserControl1.AllowBorderColorChanges = false;
            this.bunifuUserControl1.AllowMouseEffects = false;
            this.bunifuUserControl1.AnimationSpeed = 200;
            this.bunifuUserControl1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuUserControl1.BackgroundColor = System.Drawing.Color.LightBlue;
            this.bunifuUserControl1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(69)))), ((int)(((byte)(155)))));
            this.bunifuUserControl1.BorderRadius = 20;
            this.bunifuUserControl1.BorderThickness = 2;
            this.bunifuUserControl1.ColorContrastOnClick = 30;
            this.bunifuUserControl1.ColorContrastOnHover = 30;
            this.bunifuUserControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.bunifuUserControl1.Image = null;
            this.bunifuUserControl1.ImageMargin = new System.Windows.Forms.Padding(0);
            this.bunifuUserControl1.Location = new System.Drawing.Point(61, 200);
            this.bunifuUserControl1.Name = "bunifuUserControl1";
            this.bunifuUserControl1.ShowBorders = false;
            this.bunifuUserControl1.Size = new System.Drawing.Size(248, 163);
            this.bunifuUserControl1.Style = Bunifu.UI.WinForms.BunifuUserControl.UserControlStyles.Flat;
            this.bunifuUserControl1.TabIndex = 4;
            this.bunifuUserControl1.Click += new System.EventHandler(this.bunifuUserControl1_Click);
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 10;
            this.bunifuElipse1.TargetControl = this;
            // 
            // AdminMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(962, 714);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdminMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminMenu";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuPictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Bunifu.UI.WinForms.BunifuPictureBox bunifuPictureBox1;
        private Bunifu.UI.WinForms.BunifuUserControl bunifuUserControl6;
        private Bunifu.UI.WinForms.BunifuUserControl bunifuUserControl8;
        private Bunifu.UI.WinForms.BunifuUserControl bunifuUserControl4;
        private Bunifu.UI.WinForms.BunifuUserControl bunifuUserControl3;
        private Bunifu.UI.WinForms.BunifuUserControl bunifuUserControl1;
        private Bunifu.UI.WinForms.BunifuPictureBox bunifuPictureBox2;
        private Bunifu.UI.WinForms.BunifuPictureBox bunifuPictureBox6;
        private Bunifu.UI.WinForms.BunifuPictureBox bunifuPictureBox5;
        private Bunifu.UI.WinForms.BunifuPictureBox bunifuPictureBox3;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton4;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton5;
    }
}