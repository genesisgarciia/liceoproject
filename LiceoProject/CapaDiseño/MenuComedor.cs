﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDiseño
{
    public partial class MenuComedor : Form
    {

        private String usu;
        private string estudiante1;
        private string estudiante2;
        public MenuComedor(String user)
        {
            InitializeComponent();
            usu = user;
            estudiante1 = "702480818";
            estudiante2 = "207610854";
        }
        private void CargarTabla1()
        {

            DataGridViewRow fila = new DataGridViewRow();
            fila.CreateCells(dgv);
            fila.Cells[0].Value = 1;
            fila.Cells[1].Value = "GENESIS";
            fila.Cells[2].Value = "GARCIA ARAYA";
            fila.Cells[3].Value = "702480818";
            
            fila.Cells[4].Value = "3123223";
            fila.Cells[5].Value = "SAN LUIS";
            fila.Cells[6].Value = "SI";
            

            dgv.Rows.Add(fila);

            
            dgv.CurrentRow.Selected = false;

        }

        private void CargarTabla2()
        {
            DataGridViewRow fila2 = new DataGridViewRow();
            fila2.CreateCells(dgv);
            fila2.Cells[0].Value = 1;
            fila2.Cells[1].Value = "DANIEL";
            fila2.Cells[2].Value = "VARGAS SIBAJA";
            fila2.Cells[3].Value = "32321331";

            fila2.Cells[4].Value = "54535";
            fila2.Cells[5].Value = "COLONIA PUNTARENAS";
            fila2.Cells[6].Value = "SI";


            dgv.Rows.Add(fila2);
            dgv.CurrentRow.Selected = false;

        }
        



        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuImageButton3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            if (usu == "2")
            {
                Login l = new Login();
                l.Show();
                this.Hide();
            }
            else {
                AdminMenu am = new AdminMenu();
                am.Show();
                this.Hide();

            }
            
            
            
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtBuscar.Text == estudiante1)
            {
                CargarTabla1();
            }
            else if (txtBuscar.Text == estudiante2)
            {
                CargarTabla2();
            }
            else if (txtBuscar.Text != estudiante2) {
                MessageBox.Show("El estudiante no existe","Atencion!");
            }
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Registro agregado con exito!", "Exito!");
            if (usu == "2")
            {
                Login l = new Login();
                l.Show();
                this.Hide();
            }
            else
            {
                AdminMenu am = new AdminMenu();
                am.Show();
                this.Hide();

            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
