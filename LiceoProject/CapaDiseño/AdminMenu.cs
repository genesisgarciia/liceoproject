﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDiseño
{
    public partial class AdminMenu : Form
    {
        public AdminMenu()
        {
            InitializeComponent();
        }

        private void bunifuUserControl8_Click(object sender, EventArgs e)
        {
            MenuUsuarios mu = new MenuUsuarios();

            mu.Show();
            this.Hide();
        }

        private void bunifuPictureBox2_Click(object sender, EventArgs e)
        {
            MenuTransporte mt = new MenuTransporte("1");
            mt.Show();
            this.Hide();

        }

        private void bunifuUserControl1_Click(object sender, EventArgs e)
        {
            MenuTransporte mt = new MenuTransporte("1");
            mt.Show();
            this.Hide();


        }

        private void bunifuPictureBox1_Click(object sender, EventArgs e)
        {
            MenuComedor mc = new MenuComedor("1");
            mc.Show();
            this.Hide();
        }

        private void bunifuUserControl4_Click(object sender, EventArgs e)
        {
            MenuComedor mc = new MenuComedor("1");
            mc.Show();
            this.Hide();
        }

        private void bunifuPictureBox6_Click(object sender, EventArgs e)
        {
            MenuUsuarios mu = new MenuUsuarios();
            
            mu.Show();
            this.Hide();
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MenuUsuarios mu = new MenuUsuarios();

            mu.Show();
            this.Hide();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MenuTransporte mt = new MenuTransporte("1");

            mt.Show();
            this.Hide();
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MenuComedor mc = new MenuComedor("1");
            mc.Show();
            this.Hide();
        }

        private void bunifuPictureBox5_Click(object sender, EventArgs e)
        {
            MenuEstudiante me = new MenuEstudiante();
            me.Show();
            this.Hide();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MenuEstudiante me = new MenuEstudiante();
            me.Show();
            this.Hide();
        }

        private void bunifuUserControl3_Click(object sender, EventArgs e)
        {
            MenuEstudiante me = new MenuEstudiante();
            me.Show();
            this.Hide();
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            Login lo = new Login();
            lo.Show();
            this.Hide();
        }

        private void bunifuImageButton4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuImageButton5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void bunifuPictureBox3_Click(object sender, EventArgs e)
        {
            Reportes r = new Reportes();
            r.Show();
            this.Hide();
        }

        private void bunifuUserControl6_Click(object sender, EventArgs e)
        {
            Reportes r = new Reportes();
            r.Show();
            this.Hide();
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Reportes r = new Reportes();
            r.Show();
            this.Hide();
        }
    }
}
