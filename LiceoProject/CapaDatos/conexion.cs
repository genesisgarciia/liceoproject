﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class Conexion
    {

        public NpgsqlConnection con = new NpgsqlConnection("Server=localhost;" +
           " User Id = postgres;" +
           " Password = postgres;" +
           " Database = liceo");


        public void Conectar() {
            try {
                con.Open();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }



        }


        // Desconectar a la Base de datos
        public void Desconectar()
        {
            try
            {//Estado
                if (con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                }
            }
            catch (Exception e)
            {//Error 
                Console.WriteLine(e);
            }
        }
  
    }
}
